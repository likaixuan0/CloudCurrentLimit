package com.lkx.enums;

/**
 * 限流类型枚举类
 */
public enum CloudLimitTypeEnum {

    TOKEN(1,"令牌桶算法"),

    LEAKY_BUCKET(2,"漏桶算法"),

    FIXED_WINDOW(3,"固定窗口算法"),

    SLIDE_WINDOW(4,"滑动窗口算法");

    private Integer type;
    private String desc;

    CloudLimitTypeEnum(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
