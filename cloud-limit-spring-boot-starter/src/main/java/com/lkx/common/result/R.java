package com.lkx.common.result;

import java.util.HashMap;

/**
 * ClassName: R
 * Function:  通用返回类型
 * Date:      2019-09-14 10:14
 * author     likaixuan
 * version    V1.0
 */
public class R extends HashMap<String,Object> {

    public R(){
        put("code",0);
        put("msg","操作成功");
    }

    public static R error(){
        return error(500,"操作失败",null);
    }

    public static R error(String msg){
        return error(500,msg,null);
    }
    public static R error(int code, String msg, Object data){
        R r = new R();
        r.put("code",code);
        r.put("msg",msg);
        r.put("data",data);
        return r;
    }

    public static R success(){
       return new R();
    }

    public static R success(String msg){
        R r = new R();
        r.put("msg",msg);
        return r;
    }
}
