package com.lkx.annotation;

import com.lkx.constants.CloudLimitConstant;
import com.lkx.enums.CloudLimitTypeEnum;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @author LiKaixuan
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Order(Ordered.HIGHEST_PRECEDENCE)
@Inherited
public @interface CloudLimitAnnottion {

    /**
     * 限流类型
     * @return
     */
    CloudLimitTypeEnum type() default CloudLimitTypeEnum.TOKEN;

    /**
     * 限流访问次数
     * @return
     */
    int limitCount() default 50;

    /**
     * 限流时间范围
     * @return
     */
    long time() default 1;

    /**
     * 限流时间范围的单位
     * @return
     */
    TimeUnit unit() default TimeUnit.SECONDS;

    /**
     *漏出水滴或令牌桶生成令牌时间间隔，单位(毫秒 type为TOKEN\LEAKY_BUCKET时生效)
     */
    long period() default 1000;

    /**
     * 每次漏出水滴数或生成令牌数(type为TOKEN\LEAKY_BUCKET时生效)
     */
    int limitPeriodCount() default 50;

    String limitMsg() default CloudLimitConstant.LIMIT_MSG;

    /**
     * 限流自定义key，如果不存在，则用class+methodName用作key
     * @return
     */
    String limitKey() default "";


}
