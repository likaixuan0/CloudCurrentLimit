package com.lkx.constants;

public interface CloudLimitConstant {

    /**
     * 令牌桶前缀
     */
    public static final String QPS_TOKEN="cloud:limit:qps:tokenBucket:";

    /**
     * 漏桶前缀
     */
    public static final String QPS_LEAKY_BUCKET= "cloud:limit:qps:leakyBucket:";

    /**
     * 固定窗口前缀
     */
    public static final String QPS_FIXED_WINDOW="cloud:limit:qps:fixedWindow:";

    /**
     * 滑动窗口前缀
     */
    public static final String QPS_SLIDE_WINDOW= "cloud:limit:qps:slideWindow:";


    /**
     * 默认限流提示语
     */
    public static final String LIMIT_MSG="触发限流了";
}
