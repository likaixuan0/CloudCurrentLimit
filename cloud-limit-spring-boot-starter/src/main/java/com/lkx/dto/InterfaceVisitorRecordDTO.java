package com.lkx.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author likaixuan
 * @since 2022-10-11
 */
@Data
public class InterfaceVisitorRecordDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    //接口url
    private String interfaceName;

    //接口url
    private String interfaceUrl;

    //访问者ip
    private String visitorIp;

    // 0:正常没被限流  1:被限流限制访问
    private Integer status;

    //访问接口时间
    private LocalDateTime visitorTime;



}
