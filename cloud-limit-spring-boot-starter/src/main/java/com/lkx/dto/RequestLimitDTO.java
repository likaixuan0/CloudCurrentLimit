package com.lkx.dto;

import com.lkx.annotation.CloudLimitAnnottion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestLimitDTO implements Serializable {

    /**
     * 限流配置
     */
    private CloudLimitAnnottion limiter;

    /**
     * 拦截键
     */
    private String key;
}
