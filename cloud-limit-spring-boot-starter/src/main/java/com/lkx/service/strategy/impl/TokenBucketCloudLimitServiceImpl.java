package com.lkx.service.strategy.impl;

import com.lkx.config.CloudCurrentLimitProperties;
import com.lkx.dto.RequestLimitDTO;
import com.lkx.enums.CloudLimitTypeEnum;
import com.lkx.service.strategy.CloudLimitService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
//@Primary
public class TokenBucketCloudLimitServiceImpl implements CloudLimitService {


    @Value("${cloud.limit.scan-package:}")
    private String scanPackage;

    private CloudCurrentLimitProperties properties;

    @Autowired
    private ResourcePatternResolver resourcePatternResolver;

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private DefaultRedisScript<Long> redisScript;


    public TokenBucketCloudLimitServiceImpl() {
    }

    public TokenBucketCloudLimitServiceImpl(CloudCurrentLimitProperties properties) {
        this.properties = properties;
    }

    @Override
    public boolean checkRequestLimit(RequestLimitDTO dto) {

        List<String> keys = new ArrayList<>();
        keys.add( dto.getKey() );
        Long result = this.redisTemplate.execute(redisScript, keys, dto.getLimiter().limitCount(), dto.getLimiter().limitCount(), System.currentTimeMillis());
        //log.info( "Access try count is {} for key={}", result, dto.getKey() );

        if (result!=null && result==0) {
            //log.info("[{}]限流控制，令牌桶中不存在令牌，请求被拦截",dto.getKey());
            return true;
        }else {
            //log.info("[{}]令牌桶存在令牌，未达到限流值，放行",dto.getKey());
            return false;
        }

    }

    @Override
    public CloudLimitTypeEnum getLimitType() {
        return CloudLimitTypeEnum.TOKEN;
    }

    /**
     * 生成令牌线程池
     * @return 线程池实例化对象
     */
    @PostConstruct
    @Bean(name = "tokenPushThreadPoolScheduler")
    public ThreadPoolTaskScheduler tokenPushThreadConfig(){
        //扫描出所有使用了自定义注解并且限流类型为令牌桶算法的方法
        List<RequestLimitDTO> list = this.getTokenLimitList(resourcePatternResolver,CloudLimitTypeEnum.TOKEN,scanPackage);
        if(null == list || list.isEmpty()){
            log.warn("未扫描到使用【{}】的方法,限流未开启...",CloudLimitTypeEnum.TOKEN.getDesc());
            return null;
        }else{
            ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
            scheduler.setPoolSize(1);
            scheduler.setThreadNamePrefix("令牌桶线程");
            log.info("=========>>>>>>>令牌桶线程池实例化");
            return scheduler;
        }
    }
}
