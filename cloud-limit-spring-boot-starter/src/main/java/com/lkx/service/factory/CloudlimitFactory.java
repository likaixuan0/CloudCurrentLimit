package com.lkx.service.factory;

import com.lkx.enums.CloudLimitTypeEnum;
import com.lkx.service.strategy.CloudLimitService;
import com.lkx.service.strategy.impl.LeakyBucketCloudLimitServiceImpl;
import com.lkx.service.strategy.impl.TokenBucketCloudLimitServiceImpl;

import java.util.HashMap;
import java.util.Map;

public class CloudlimitFactory {

    private static final Map<CloudLimitTypeEnum, CloudLimitService> payStrategies = new HashMap<>();

    static {
        payStrategies.put(CloudLimitTypeEnum.TOKEN, new TokenBucketCloudLimitServiceImpl());
        payStrategies.put(CloudLimitTypeEnum.LEAKY_BUCKET, new LeakyBucketCloudLimitServiceImpl());
    }

    public static CloudLimitService getCloudLimitType(CloudLimitTypeEnum cloudLimitTypeEnum) {
        if (cloudLimitTypeEnum == null) {
            throw new IllegalArgumentException(cloudLimitTypeEnum + " type is empty.");
        }
        if (!payStrategies.containsKey(cloudLimitTypeEnum)) {
            throw new IllegalArgumentException(cloudLimitTypeEnum.getDesc() + " type not supported.");
        }
        return payStrategies.get(cloudLimitTypeEnum);
    }
}
