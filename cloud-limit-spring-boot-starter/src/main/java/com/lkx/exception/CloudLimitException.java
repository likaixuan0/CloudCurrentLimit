package com.lkx.exception;

public class CloudLimitException extends RuntimeException{


    public CloudLimitException(String message) {
        super(message);
    }

}
