package com.lkx.visit.constant;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * ClassName: VisitConstant
 * Function:
 * Date:      2022-01-14 11:20
 * author     likaixuan
 * version    V1.0
 */
public class VisitConstant {

    private static AtomicLong atomicLong;
    public static long preMin;
    public static long minCount;


    public static HashMap<String,Object> visitHashMap;

    static {
        atomicLong = new AtomicLong(0);
        preMin=0L;
        minCount = 0L;
        visitHashMap = new HashMap();
    }

    public static void reset(){
        atomicLong = new AtomicLong(0);
        minCount = 0L;
    }


    public static long addOne(){
        minCount = atomicLong.incrementAndGet();
        return minCount;
    }


}
