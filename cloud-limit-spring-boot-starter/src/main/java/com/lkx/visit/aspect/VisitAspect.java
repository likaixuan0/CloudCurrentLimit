package com.lkx.visit.aspect;

import com.lkx.visit.constant.VisitConstant;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


/**
 * ClassName: VisitAspect
 * Function:
 * Date:      2023-01-14 11:13
 * author     likaixuan
 * version    V1.0
 */
@Component
@Aspect
public class VisitAspect {


    // Service
    @Pointcut("execution(* com.lkx..*Controller.*(..))")
    public void visitPointCut() {

    }


    @Before("visitPointCut()")
    public void doBefore(JoinPoint joinPoint) {
        add(joinPoint);
    }


    private void add(JoinPoint joinPoint){
        long min =  System.currentTimeMillis()/1000;
        String questStr = joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName();
       if(VisitConstant.preMin==0){
            VisitConstant.preMin = min;
        }
        if(questStr.indexOf("see")==-1 && questStr.indexOf("init")==-1) {
            VisitConstant.addOne();
            /*if(Long.compare(min,VisitConstant.preMin)==1){
                System.out.println("min:"+min+" minCount:"+VisitConstant.minCount);
                redisUtils.set("sec:"+min,VisitConstant.minCount,60L);
                VisitConstant.visitHashMap.put("sec:"+min,VisitConstant.minCount);
                VisitConstant.reset();
            }*/
        }


    }

}
