package com.lkx.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class ThreadPoolConfig {

    /**
     * 生成令牌线程池
     * @return 线程池实例化对象rug
     */
//    @Bean(name = "tokenPushThreadPoolScheduler")
//    public ThreadPoolTaskScheduler tokenPushThreadConfig(){
//        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
//        scheduler.setPoolSize(1);
//        scheduler.setThreadNamePrefix("生成令牌线程");
//        log.info("=========>>>>>>>生成令牌线程池实例化");
//        return scheduler;
//    }

    /**
     * 漏桶线程池
     * @return 线程池实例化对象
     */
//    @Bean(name = "leakyBucketPopThreadPoolScheduler")
//    public ThreadPoolTaskScheduler leakyBucketPopThreadConfig(){
//        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
//        scheduler.setPoolSize(1);
//        scheduler.setThreadNamePrefix("漏桶线程");
//        log.info("=========>>>>>>>生成漏桶线程池实例化");
//        return scheduler;
//    }
}
