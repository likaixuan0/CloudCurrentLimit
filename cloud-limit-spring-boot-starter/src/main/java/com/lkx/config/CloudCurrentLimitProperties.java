package com.lkx.config;
import com.lkx.enums.CloudLimitTypeEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = CloudCurrentLimitProperties.CLOUD_CURRENT_LIMIT_PREFIX)
public class CloudCurrentLimitProperties {

    //配置前缀
    public static final String CLOUD_CURRENT_LIMIT_PREFIX = "cloud.limit";


    private CloudLimitTypeEnum type;

    /**
     * 限流访问次数
     */
    private int limitCount=50;

    /**
     * 限流时间范围
     */
    private long time=1;


    /**
     *漏出水滴或令牌桶生成令牌时间间隔，单位(毫秒 type为TOKEN\LEAKY_BUCKET时生效)
     */
    private long period=1000;

    /**
     * 每次漏出水滴数或生成令牌数(type为TOKEN\LEAKY_BUCKET时生效)
     */
    private int limitPeriodCount=50;

    /**
     * 需要限流扫描的包
     */
    private String scanPackage;

    public String getRecordLimitUrl() {
        return recordLimitUrl;
    }

    public void setRecordLimitUrl(String recordLimitUrl) {
        this.recordLimitUrl = recordLimitUrl;
    }

    /**
     * 记录接口访问次数以及限流次数地址
     * @return
     */
    private String recordLimitUrl;

}
