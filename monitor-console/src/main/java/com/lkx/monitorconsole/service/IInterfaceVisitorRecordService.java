package com.lkx.monitorconsole.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lkx.monitorconsole.entity.InterfaceVisitorRecord;
import com.lkx.monitorconsole.params.InterfaceVisitorRecordParams;
import com.lkx.monitorconsole.params.InterfaceVisitorRecordQueryParams;
import com.lkx.monitorconsole.utils.R;
import com.lkx.monitorconsole.vo.RecordCountVO;
import com.lkx.monitorconsole.vo.VisitorLimitCountVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author likaixuan
 * @since 2022-10-11
 */
public interface IInterfaceVisitorRecordService extends IService<InterfaceVisitorRecord> {


    R<String> asynAddList(InterfaceVisitorRecordParams params);

    R<String> batchAdd(List<InterfaceVisitorRecord> records);

    R<RecordCountVO> queryLastlist();

    R<VisitorLimitCountVO> queryMinlist(InterfaceVisitorRecordQueryParams params);

    R<VisitorLimitCountVO> queryHourlist(InterfaceVisitorRecordQueryParams params);

    R<VisitorLimitCountVO> queryDaylist(InterfaceVisitorRecordQueryParams params);

    R<VisitorLimitCountVO> queryMonlist(InterfaceVisitorRecordQueryParams params);

    R<VisitorLimitCountVO> querylist(InterfaceVisitorRecordQueryParams params);


}
