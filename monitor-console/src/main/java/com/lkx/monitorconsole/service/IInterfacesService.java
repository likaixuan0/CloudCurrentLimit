package com.lkx.monitorconsole.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lkx.monitorconsole.entity.Interfaces;
import com.lkx.monitorconsole.params.InterfaceParams;
import com.lkx.monitorconsole.utils.R;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author likaixuan
 * @since 2022-10-12
 */
public interface IInterfacesService extends IService<Interfaces> {

    R<String> addList(InterfaceParams params);

    R<String> add(Interfaces interfaces);

}
