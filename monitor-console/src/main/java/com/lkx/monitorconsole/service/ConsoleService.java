package com.lkx.monitorconsole.service;

import com.lkx.monitorconsole.vo.SystemInfoVO;

public interface ConsoleService {

    SystemInfoVO systemMonitor();
}
