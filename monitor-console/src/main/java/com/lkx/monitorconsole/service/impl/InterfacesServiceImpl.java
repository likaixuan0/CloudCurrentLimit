package com.lkx.monitorconsole.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lkx.monitorconsole.config.RabbitConfig;
import com.lkx.monitorconsole.constants.RabbitConstant;
import com.lkx.monitorconsole.entity.Interfaces;
import com.lkx.monitorconsole.mapper.InterfacesMapper;
import com.lkx.monitorconsole.params.InterfaceParams;
import com.lkx.monitorconsole.rabbitmq.DirectSender;
import com.lkx.monitorconsole.schedulers.InterfaceScheduler;
import com.lkx.monitorconsole.service.IInterfacesService;
import com.lkx.monitorconsole.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author likaixuan
 * @since 2022-10-12
 */
@Slf4j
@Service
public class InterfacesServiceImpl extends ServiceImpl<InterfacesMapper, Interfaces> implements IInterfacesService {

    @Autowired
    private InterfacesMapper mapper;

    @Autowired
    private DirectSender mqSender;


    @Async
    @Override
    public R<String> addList(InterfaceParams params) {

        if(InterfaceScheduler.list.size()>RabbitConstant.interfaceCount){
            log.info("本次数据通过mq通道下发");
            mqSender.send(params);
        }else{
            log.info("本次数据通过内存通道下发");
            Interfaces interfaces = new Interfaces();
            BeanUtils.copyProperties(params,interfaces);
            interfaces.setCreateTime(LocalDateTime.now());
            InterfaceScheduler.list.add(interfaces);
        }
        return R.ok();
    }

    @Override
    public R<String> add(Interfaces interfaces) {
        return R.ok(mapper.insert(interfaces));
    }
}
