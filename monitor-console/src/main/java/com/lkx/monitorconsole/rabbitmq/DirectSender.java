package com.lkx.monitorconsole.rabbitmq;

import com.lkx.monitorconsole.config.RabbitConfig;
import com.lkx.monitorconsole.params.InterfaceParams;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ClassName: DirectSender
 * Function:  TODO
 * Date:      2022-11-6 17:39:23
 * @author     likaixuan
 * version    V1.0
 */
@Component
public class DirectSender {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(InterfaceParams params) {
        this.rabbitTemplate.convertAndSend(RabbitConfig.DIRECT_EXCHANGE, RabbitConfig.DIRECT_ROUT_KEY, params);
    }
}
