package com.lkx.monitorconsole.rabbitmq;

import com.lkx.monitorconsole.config.RabbitConfig;
import com.lkx.monitorconsole.params.InterfaceParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * ClassName: DirectReceiver
 * Function:  TODO
 * Date:      2022/12/19 21:35
 * @author     likaixuan
 * version    V1.0
 */
@Component
public class DirectListen {

    private static Logger logger = LoggerFactory.getLogger(DirectListen.class);

    // queues是指要监听的队列的名字
    @RabbitListener(queues = RabbitConfig.DIRECT_QUEUE1)
    public void receiveDirect1(InterfaceParams params) throws Exception {

        logger.info("【receiveDirect1监听到消息】" + params);
        //throw new Exception("receiveDirect1监听到消息异常1：..");
    }

    @RabbitListener(queues = RabbitConfig.DIRECT_QUEUE2)
    public void receiveDirect2(InterfaceParams params) throws Exception {

        logger.info("【receiveDirect2监听到消息】" + params);
        //throw new Exception("receiveDirect1监听到消息异常2。。");
    }
}
