package com.lkx.monitorconsole.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
@Slf4j
public class ThreadPoolConfig {

    /**
     * 写入接口请求信息线程池
     * @return 线程池实例化对象rug
     */
    @Bean(name = "interfaceRecordPushThreadPoolScheduler")
    public ThreadPoolTaskScheduler interfaceRecordPushThreadConfig(){
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(1);
        scheduler.setThreadNamePrefix("写入接口请求信息线程");
        log.info("=========>>>>>>>写入接口请求信息线程池实例化");
        return scheduler;
    }

    /**
     * 写写入限流接口信息线程池
     * @return 线程池实例化对象rug
     */
    @Bean(name = "interfacePushThreadPoolScheduler")
    public ThreadPoolTaskScheduler interfacePushThreadConfig(){
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(1);
        scheduler.setThreadNamePrefix("写入限流接口信息线程");
        log.info("=========>>>>>>>写入限流接口信息线程池实例化");
        return scheduler;
    }

}
