package com.lkx.monitorconsole.utils;

import lombok.Data;

/**
 * @author likaixuan
 */
@Data
public class R<T> {
    private static final long serialVersionUID = 1L;

    private Integer code;
    private String msg;
    private Object data;
    private Integer total;
    private Integer totalPage;
    private Integer currentPage;

    public R() {
        code = 200;
        msg = "操作成功";
    }

    public static R error() {
        return error(500, "操作失败");
    }

    public static R error(String msg) {
        return error(500, msg);
    }

    public static R error(int code, String msg) {
        R r = new R();
        r.code=code;
        r.msg=msg;
        return r;
    }
    public static R error(int code, String msg,Object data) {
        R r = new R();
        r.code=code;
        r.msg=msg;
        r.data = data;
        return r;
    }


    public static R ok(String msg) {
        R r = new R();
        r.msg=msg;
        return r;
    }

    public static R ok(Object object) {
        R r = new R();
        r.data=object;
        return r;
    }


    public static R ok() {
        return new R();
    }


}
