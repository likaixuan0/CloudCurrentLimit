package com.lkx.monitorconsole;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@MapperScan("com.lkx.monitorconsole.mapper")
@EnableAsync
@SpringBootApplication
public class MonitorConsoleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonitorConsoleApplication.class, args);
    }

}
