package com.lkx.monitorconsole.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class InterfaceParams implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "接口名称")
    private String interfaceName;

    @ApiModelProperty(value = "接口path")
    private String interfaceUrl;

    @ApiModelProperty(value = "限流类型")
    private String limitType;

    @ApiModelProperty(value = "限流单位(秒)")
    private Integer unitTime;

    @ApiModelProperty(value = "单位时间可访问次数")
    private Integer limitCount;

}
