package com.lkx.monitorconsole.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class InterfaceVisitorRecordParams implements Serializable {
    @ApiModelProperty(value = "接口名称")
    private String interfaceName;

    @ApiModelProperty(value = "接口url")
    private String interfaceUrl;

    @ApiModelProperty(value = "访问者ip")
    private String visitorIp;

    @ApiModelProperty(value = "0:正常没被限流  1:被限流限制访问")
    private Integer status;

    @ApiModelProperty(value = "访问接口时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime visitorTime;


}
