package com.lkx.monitorconsole.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel("接口请求记录查询类")
public class InterfaceVisitorRecordQueryParams implements Serializable {

    @ApiModelProperty("访问接口开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date visitorStartTime;

    @ApiModelProperty("访问接口结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date visitorEndTime;

    @ApiModelProperty("接口名称")
    private String interfaceName;

    @ApiModelProperty("接口path")
    private String interfaceUrl;


}
