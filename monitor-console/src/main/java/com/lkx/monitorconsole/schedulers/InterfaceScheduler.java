package com.lkx.monitorconsole.schedulers;

import com.lkx.monitorconsole.entity.InterfaceVisitorRecord;
import com.lkx.monitorconsole.entity.Interfaces;
import com.lkx.monitorconsole.service.IInterfacesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class InterfaceScheduler {


    public static List<Interfaces> list = new ArrayList<>();

    public static List<InterfaceVisitorRecord> recordList = new ArrayList<>();


    static {
        Interfaces interfaces = new Interfaces();
        interfaces.setInterfaceName("init");
        list.add(interfaces);
    }


    @Resource(name = "interfacePushThreadPoolScheduler")
    private ThreadPoolTaskScheduler scheduler;
    @Autowired
    private IInterfacesService service;

    @PostConstruct
    public void pushInterFace(){

        scheduler.scheduleAtFixedRate(() -> {
            if (list.size() == 1) {
                //log.info("=======>>>>>>开始消费，目前队列中没有数据待处理");
                return;
            }
            if(list.size()>1){
                log.info("=======>>>>>>开始消费，目前队列中有{}个数据待处理", list.size()-1);
                log.info("=======>>>>>>消费结果:{}", service.add(list.get(1)).toString());
                list.remove(1);
                log.info("=======>>>>>>消费结束，目前队列中还有{}个数据待处理", list.size()-1);
            }
        },1000);

    }


}
