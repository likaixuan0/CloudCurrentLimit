package com.lkx.monitorconsole.schedulers;

import com.lkx.monitorconsole.entity.InterfaceVisitorRecord;
import com.lkx.monitorconsole.service.IInterfaceVisitorRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class InterfaceRecordScheduler {

    public static List<InterfaceVisitorRecord> list = new ArrayList<>();

    static {
        InterfaceVisitorRecord record = new InterfaceVisitorRecord();
        record.setInterfaceName("init");
        list.add(record);
    }


    @Resource(name = "interfaceRecordPushThreadPoolScheduler")
    private ThreadPoolTaskScheduler scheduler;

    @Autowired
    private IInterfaceVisitorRecordService service;

    private List<InterfaceVisitorRecord> templist;


    @PostConstruct
    public void pushInterFace(){

        scheduler.scheduleAtFixedRate(() -> {
            if (list.size() == 1) {
                //log.info("=======>>>>>>开始消费222，目前队列中没有数据待处理");
                return;
            }

            templist = new ArrayList<>();
            if(list.size()>100){
                templist.addAll(list.subList(1,100));
            }else{
                templist.addAll(list.subList(1,list.size()));
            }
            log.info("=======>>>>>>开始消费222，目前队列中有{}个数据待处理", list.size()-1);

            log.info("=======>>>>>>消费结果222:{}", service.batchAdd(templist).toString());
            list.removeAll(templist);
            log.info("=======>>>>>>消费结束222，目前队列中还有{}个数据待处理", list.size()-1);

        },1000);

    }
}
