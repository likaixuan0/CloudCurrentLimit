package com.lkx.monitorconsole.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SystemInfoVO implements Serializable {

    @ApiModelProperty("系统名称")
    private String osName;

    @ApiModelProperty("cpu总核心数")
    private double totalCpuCore;

    @ApiModelProperty("cpu总线程数")
    private double totalCpuThread;

    @ApiModelProperty("cpu占用率")
    private double cpuLoad;

    @ApiModelProperty("系统总内存")
    private double totalMemory;

    @ApiModelProperty("剩余内存")
    private double freeMemory;

}
