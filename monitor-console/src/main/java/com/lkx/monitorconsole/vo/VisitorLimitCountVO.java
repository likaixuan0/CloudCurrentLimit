package com.lkx.monitorconsole.vo;

import lombok.Data;

import java.util.Date;

@Data
public class VisitorLimitCountVO {

    private Date visitorTime;

    private String visitorTimeStr;

    private int count;

    private int limitCount;

    private int status;
}
