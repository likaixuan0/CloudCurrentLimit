package com.lkx.monitorconsole.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class RecordCountVO implements Serializable {

    private Long minCount;

    private Long hourCount;

    private Long dayCount;

    private Long monCount;


}
