package com.lkx.monitorconsole.vo;


import lombok.Data;

@Data
public class VisitorLimitVO {

    private String[] names;
    private Long[] visitorData;
    private Long[] limitData;
}
