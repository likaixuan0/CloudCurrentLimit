package com.lkx.monitorconsole.controller;

import com.lkx.monitorconsole.service.ConsoleService;
import com.lkx.monitorconsole.utils.R;
import com.lkx.monitorconsole.vo.SystemInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ConsoleController {

    @Autowired
    private ConsoleService consoleService;

    @RequestMapping("/")
    public String index(){
        return "console/index";
    }

    @RequestMapping("/console/home/console")
    public String console(){
        return "console/home/console";
    }

    @RequestMapping("/console/home/homepage1")
    public String homepage1(){
        return "console/home/homepage1";
    }

    @RequestMapping("/console/home/homepage2")
    public String homepage2(){
        return "console/home/homepage2";
    }

    @ResponseBody
    @GetMapping("/getSystemInfo")
    public R<SystemInfoVO> getSystemInfo() {
        return R.ok(consoleService.systemMonitor());
    }


}
