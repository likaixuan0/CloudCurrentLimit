package com.lkx.monitorconsole.controller;


import com.lkx.monitorconsole.params.InterfaceParams;
import com.lkx.monitorconsole.service.IInterfacesService;
import com.lkx.monitorconsole.utils.R;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author likaixuan
 * @since 2022-10-12
 */
@RestController
@RequestMapping("/interfaces")
public class InterfacesController {

    @Autowired
    private IInterfacesService service;

    @GetMapping("/addList")
    public R addList(InterfaceParams params){
        service.addList(params);
        return R.ok(DateFormatUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
    }

}
