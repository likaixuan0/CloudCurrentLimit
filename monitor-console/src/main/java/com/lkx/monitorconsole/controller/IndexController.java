package com.lkx.monitorconsole.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lkx.monitorconsole.entity.InterfaceVisitorRecord;
import com.lkx.monitorconsole.mapper.InterfaceVisitorRecordExtendMapper;
import com.lkx.monitorconsole.utils.R;
import com.lkx.monitorconsole.vo.RecordCountVO;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private InterfaceVisitorRecordExtendMapper mapper;

    @GetMapping("/index")
    public R<RecordCountVO> index(){
        RecordCountVO vo = new RecordCountVO();

        QueryWrapper<InterfaceVisitorRecord> query = new QueryWrapper<InterfaceVisitorRecord>();
        query.ge("create_time", DateUtils.addMinutes(new Date(),-1));
        query.lt("create_time",new Date());
        vo.setMinCount(mapper.selectCount(query));

        query.clear();
        query.ge("create_time", DateUtils.addMinutes(new Date(),-60));
        query.lt("create_time",new Date());
        vo.setHourCount(mapper.selectCount(query));

        query.clear();
        query.ge("create_time", DateUtils.addHours(new Date(),-24));
        query.lt("create_time",new Date());
        vo.setDayCount(mapper.selectCount(query));

        query.clear();
        query.ge("create_time", DateUtils.addMonths(new Date(),-1));
        query.lt("create_time",new Date());
        vo.setMonCount(mapper.selectCount(query));
        return R.ok(vo);
    }
}
