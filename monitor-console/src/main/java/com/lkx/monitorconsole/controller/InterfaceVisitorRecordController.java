package com.lkx.monitorconsole.controller;


import com.lkx.monitorconsole.entity.InterfaceVisitorRecord;
import com.lkx.monitorconsole.mapper.InterfaceVisitorRecordMapper;
import com.lkx.monitorconsole.params.InterfaceVisitorRecordParams;
import com.lkx.monitorconsole.params.InterfaceVisitorRecordQueryParams;
import com.lkx.monitorconsole.service.IInterfaceVisitorRecordService;
import com.lkx.monitorconsole.utils.IPUtils;
import com.lkx.monitorconsole.utils.R;
import com.lkx.monitorconsole.vo.RecordCountVO;
import com.lkx.monitorconsole.vo.VisitorLimitCountVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author likaixuan
 * @since 2022-10-11
 */
@RestController
@RequestMapping("/interface-visitor-record")
@Api(tags = "接口访问记录")
public class InterfaceVisitorRecordController {


    @Autowired
    private IInterfaceVisitorRecordService service;

    @Autowired
    private InterfaceVisitorRecordMapper mapper;


    @PostMapping("/asynAddList")
    public R asynAddList(HttpServletRequest request, @RequestBody InterfaceVisitorRecordParams params){
        params.setVisitorIp(IPUtils.getIpAddr(request));
        service.asynAddList(params);
        return R.ok();
    }

    @PostMapping("/add")
    public R add(HttpServletRequest request, @RequestBody InterfaceVisitorRecord record){
        record.setVisitorIp(IPUtils.getIpAddr(request));
        record.setCreateTime(LocalDateTime.now());
        if(mapper.insert(record)>0){
            return R.ok();
        }
        return R.error();
    }

    @GetMapping("/queryLastlist")
    public R<RecordCountVO> queryLastlist(InterfaceVisitorRecordQueryParams params){
        return service.queryLastlist();
    }

    @GetMapping("/queryMinlist")
    public R<VisitorLimitCountVO> queryMinlist(InterfaceVisitorRecordQueryParams params){
        return service.queryMinlist(params);
    }

    @GetMapping("/queryHourlist")
    public R<VisitorLimitCountVO> queryHourlist(InterfaceVisitorRecordQueryParams params){
        return service.queryHourlist(params);
    }

    @GetMapping("/queryDaylist")
    public R<VisitorLimitCountVO> queryDaylist(InterfaceVisitorRecordQueryParams params){
        return service.queryDaylist(params);
    }

    @GetMapping("/queryMonlist")
    public R<VisitorLimitCountVO> queryMonlist(InterfaceVisitorRecordQueryParams params){
        return service.queryMonlist(params);
    }

    @GetMapping("/querylist")
    public R<VisitorLimitCountVO> querylist(InterfaceVisitorRecordQueryParams params){
        return service.querylist(params);
    }


}
