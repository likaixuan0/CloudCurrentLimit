package com.lkx.monitorconsole.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author likaixuan
 * @since 2022-10-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("cloudlimit_interface_visitor_record")
@ApiModel(value="InterfaceVisitorRecord对象", description="")
public class InterfaceVisitorRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "接口名称")
    private String interfaceName;

    @ApiModelProperty(value = "接口url")
    private String interfaceUrl;

    @ApiModelProperty(value = "访问者ip")
    private String visitorIp;

    @ApiModelProperty(value = "0:正常没被限流  1:被限流限制访问")
    private Integer status;

    @ApiModelProperty(value = "访问接口时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime visitorTime;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;


}
