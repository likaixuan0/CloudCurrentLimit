package com.lkx.monitorconsole.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author likaixuan
 * @since 2022-10-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("cloudlimit_interfaces")
@ApiModel(value="Interfaces对象", description="")
public class Interfaces implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "接口名称")
    private String interfaceName;

    @ApiModelProperty(value = "接口path")
    private String interfaceUrl;

    @ApiModelProperty(value = "限流类型")
    private String limitType;

    @ApiModelProperty(value = "限流单位(秒)")
    private Integer unitTime;

    @ApiModelProperty(value = "单位时间可访问次数")
    private Integer limitCount;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;


}
