package com.lkx.monitorconsole.mapper;

import com.lkx.monitorconsole.entity.Interfaces;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author likaixuan
 * @since 2022-10-12
 */
public interface InterfacesMapper extends BaseMapper<Interfaces> {

}
