package com.lkx.monitorconsole.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lkx.monitorconsole.entity.InterfaceVisitorRecord;
import com.lkx.monitorconsole.params.InterfaceVisitorRecordQueryParams;
import com.lkx.monitorconsole.vo.VisitorLimitCountVO;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author likaixuan
 * @since 2022-10-11
 */
public interface InterfaceVisitorRecordExtendMapper extends BaseMapper<InterfaceVisitorRecord> {


    int batchInsert(List<InterfaceVisitorRecord> list);

    List<VisitorLimitCountVO> selectMinList(InterfaceVisitorRecordQueryParams params);

    List<VisitorLimitCountVO> selectHourList(InterfaceVisitorRecordQueryParams params);

    List<VisitorLimitCountVO> selectDayList(InterfaceVisitorRecordQueryParams params);

    List<VisitorLimitCountVO> selectMonList(InterfaceVisitorRecordQueryParams params);

    List<VisitorLimitCountVO> selectFiveMinList(InterfaceVisitorRecordQueryParams params);

    List<VisitorLimitCountVO> selectOneHourList(InterfaceVisitorRecordQueryParams params);

}
