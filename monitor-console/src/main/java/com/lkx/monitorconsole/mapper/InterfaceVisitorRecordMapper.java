package com.lkx.monitorconsole.mapper;

import com.lkx.monitorconsole.entity.InterfaceVisitorRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author likaixuan
 * @since 2022-10-11
 */
public interface InterfaceVisitorRecordMapper extends BaseMapper<InterfaceVisitorRecord> {

}
