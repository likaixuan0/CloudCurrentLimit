package com.lkx.monitorconsole;

import com.lkx.util.Excel;
import com.lkx.util.ExcelUtil;
import lombok.Data;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Data
public class TestExcel {
    @Excel(title = "十大管理")
    private String managerTitle;

    @Excel(title = "子过程")
    private String sonTitle;

    @Excel(title = "输入")
    private String inputAttr;

    @Excel(title = "工具")
    private String toolAttr;

    @Excel(title = "输出")
    private String outAttr;

    public static void main(String[] args) {
        readTxtFile("D:\\applog\\高项ITTO.txt");
    }

    public static void readTxtFile(String filePath){
        try{
            String encoding="GBK";
            File file=new File(filePath);
            if(file.isFile() && file.exists()){
                InputStreamReader read=new InputStreamReader(new FileInputStream(file),encoding);
                BufferedReader bufferedReader=new BufferedReader(read);
                String lineTxt=null;

                List<TestExcel> list = new ArrayList<>();
                TestExcel testExcel = null;
                int num = 0;
                String sumManagerTitle = null;

                while((lineTxt=bufferedReader.readLine())!=null){
                    System.out.println(lineTxt);
                    if(StringUtils.isBlank(lineTxt.replaceAll(" ",""))){
                        continue;
                    }
                    if(lineTxt.contains("--")){
                        testExcel = new TestExcel();
                        sumManagerTitle = lineTxt.replaceAll("--","");
                        testExcel.setManagerTitle(sumManagerTitle);
                        continue;
                    }
                    if(lineTxt.contains("-")){
                        if(num>0){
                            testExcel = new TestExcel();
                            testExcel.setManagerTitle(sumManagerTitle);
                        }
                        num++;
                        testExcel.setSonTitle(lineTxt.replaceAll("-",""));
                        continue;
                    }
                    if(StringUtils.isBlank(testExcel.getInputAttr())){
                        testExcel.setInputAttr(lineTxt);
                        continue;
                    }
                    if(StringUtils.isBlank(testExcel.getToolAttr())){
                        testExcel.setToolAttr(lineTxt);
                        continue;
                    }
                    if(StringUtils.isBlank(testExcel.getOutAttr())){
                        testExcel.setOutAttr(lineTxt);
                        list.add(testExcel);
                        continue;
                    }
                }
                read.close();
                ExcelUtil.exportExcel("D:\\applog\\itto.xlsx",list, TestExcel.class);
            }else
            {
                System.out.println("找不到指定的文件");
            }
        }catch (Exception e){
            System.out.println("读取文件内容出错");
            e.printStackTrace();
        }
    }

}
