package com.example.examplespringboot.controller;


import com.example.examplespringboot.utils.R;
import com.lkx.annotation.CloudLimitAnnottion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    private Logger log = LoggerFactory.getLogger(TestController.class);

    @GetMapping("/test")
    @CloudLimitAnnottion(limitCount = 2,limitKey = "limit:test",limitMsg = "你被精准限流了")
    public R limit(int a) throws Exception {
        if(a==1){
            throw new Exception("ssss异常啊啊啊啊啊");
        }
        return R.ok("hello world-世界");
    }

    @GetMapping("/testlkx")
    @CloudLimitAnnottion(limitCount = 2,limitMsg = "你被精准限流了Bylkx")
    public R limitLkxTest(int a) throws Exception {
        if(a==1){
            throw new Exception("ssss异常啊啊啊啊啊");
        }
        return R.ok("hello world-世界");
    }
}
